﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UsernameStatus.BackColor = Color.Red;
            if(UsernameBox.TextLength >= 5)
            {
                // MessageBox.Show(""+UsernameBox.TextLength);
                // MessageBox.Show("beep");
                UsernameStatus.BackColor = Color.Green;
            }
        }

        private void Password2Box_TextChanged(object sender, EventArgs e)
        {
            // check to see if the text lenth is at least 5 chars long.
            PasswordRetryStatus.BackColor = Color.Red;
            if(Password2Box.Text == Password1Box.Text)
            {
                PasswordRetryStatus.BackColor = Color.Green;
            }

        }

        private void UsernameBox_Validating(object sender, CancelEventArgs e)
        {
            // this is called once the box has left focus
        }

        private void UsernameBox_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            MessageBox.Show("Box");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // check to see if the password is correct
            if(Password1Box.Text == Password2Box.Text && UsernameBox != null)
            {
                Random rand = new Random();
                String SQLInsertStatement = "INSERT INTO dbo.FabUsers (UserID, UserName, Password) VALUES ('"+ rand.Next(100, 99999)+"','" + UsernameBox.Text + "', '" + Password1Box.Text +"')";
                MessageBox.Show(SQLInsertStatement);
                // password is correct
                // insert into the users table

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = @"Data Source=DESKTOP-BVD8FS0\SQLEXPRESS;Initial Catalog=TheFabDB;Integrated Security=True";

                conn.Open();

                // the SqlDataAdapter class has 2 args, sql statement and a SqlConnection connection
                SqlDataAdapter sda = new SqlDataAdapter();

                // create a data table
                DataTable dT = new DataTable();

                // fill the data table with the result of the data adapter
                try
                {
                    SqlCommand sqlInsert = new SqlCommand(SQLInsertStatement, conn);    // the sqlCommand class takes in SQL statement and am SQLConnection
                   // sda.InsertCommand = sqlInsert;                                      // The data adapter then takes an Insercommand 
                    sqlInsert.ExecuteNonQuery();

                    MessageBox.Show("User: "+ UsernameBox.Text +" Created");
                    this.Hide();
                    Form1 login = new Form1();
                    login.Show();
                }
                catch (System.Data.SqlClient.SqlException ex1)
                {
                    MessageBox.Show("Error"+ ex1.ToString());
                    throw;
                }
            }
            else if(Password1Box.Text != Password2Box.Text)
            {
                // then password does not match
            }
            else if(UsernameBox != null)
            {
                // username not taken
            }

        }

        private void Password1Box_TextChanged(object sender, EventArgs e)
        {
            PasswordStatus.BackColor = Color.Red;
            if (Password1Box.TextLength >= 5)
            {
                // MessageBox.Show(""+UsernameBox.TextLength);
                // MessageBox.Show("beep");
                PasswordStatus.BackColor = Color.Green;
            }
        }
    }
}
