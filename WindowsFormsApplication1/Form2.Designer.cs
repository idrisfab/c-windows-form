﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameBox = new System.Windows.Forms.TextBox();
            this.Password1Box = new System.Windows.Forms.TextBox();
            this.Password2Box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.status_label = new System.Windows.Forms.Label();
            this.UsernameStatus = new System.Windows.Forms.Label();
            this.PasswordStatus = new System.Windows.Forms.Label();
            this.PasswordRetryStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // UsernameBox
            // 
            this.UsernameBox.Location = new System.Drawing.Point(242, 56);
            this.UsernameBox.Name = "UsernameBox";
            this.UsernameBox.Size = new System.Drawing.Size(152, 20);
            this.UsernameBox.TabIndex = 0;
            this.UsernameBox.ModifiedChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.UsernameBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.UsernameBox.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.UsernameBox_GiveFeedback);
            this.UsernameBox.Validating += new System.ComponentModel.CancelEventHandler(this.UsernameBox_Validating);
            // 
            // Password1Box
            // 
            this.Password1Box.Location = new System.Drawing.Point(242, 82);
            this.Password1Box.Name = "Password1Box";
            this.Password1Box.PasswordChar = 'x';
            this.Password1Box.Size = new System.Drawing.Size(152, 20);
            this.Password1Box.TabIndex = 1;
            this.Password1Box.TextChanged += new System.EventHandler(this.Password1Box_TextChanged);
            // 
            // Password2Box
            // 
            this.Password2Box.Location = new System.Drawing.Point(242, 109);
            this.Password2Box.Name = "Password2Box";
            this.Password2Box.PasswordChar = 'x';
            this.Password2Box.Size = new System.Drawing.Size(151, 20);
            this.Password2Box.TabIndex = 2;
            this.Password2Box.TextChanged += new System.EventHandler(this.Password2Box_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(181, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "User name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(186, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(149, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Repeat password";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(319, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(115, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Register for an account";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.Register;
            this.pictureBox1.Location = new System.Drawing.Point(24, 46);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 122);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // status_label
            // 
            this.status_label.AutoSize = true;
            this.status_label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(240)))));
            this.status_label.ForeColor = System.Drawing.Color.Green;
            this.status_label.Location = new System.Drawing.Point(178, 145);
            this.status_label.Name = "status_label";
            this.status_label.Size = new System.Drawing.Size(61, 13);
            this.status_label.TabIndex = 9;
            this.status_label.Text = "Status area";
            // 
            // UsernameStatus
            // 
            this.UsernameStatus.AutoSize = true;
            this.UsernameStatus.Location = new System.Drawing.Point(400, 59);
            this.UsernameStatus.Name = "UsernameStatus";
            this.UsernameStatus.Size = new System.Drawing.Size(16, 13);
            this.UsernameStatus.TabIndex = 10;
            this.UsernameStatus.Text = "   ";
            // 
            // PasswordStatus
            // 
            this.PasswordStatus.AutoSize = true;
            this.PasswordStatus.Location = new System.Drawing.Point(401, 84);
            this.PasswordStatus.Name = "PasswordStatus";
            this.PasswordStatus.Size = new System.Drawing.Size(16, 13);
            this.PasswordStatus.TabIndex = 11;
            this.PasswordStatus.Text = "   ";
            // 
            // PasswordRetryStatus
            // 
            this.PasswordRetryStatus.AutoSize = true;
            this.PasswordRetryStatus.Location = new System.Drawing.Point(401, 112);
            this.PasswordRetryStatus.Name = "PasswordRetryStatus";
            this.PasswordRetryStatus.Size = new System.Drawing.Size(16, 13);
            this.PasswordRetryStatus.TabIndex = 12;
            this.PasswordRetryStatus.Text = "   ";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(431, 195);
            this.Controls.Add(this.PasswordRetryStatus);
            this.Controls.Add(this.PasswordStatus);
            this.Controls.Add(this.UsernameStatus);
            this.Controls.Add(this.status_label);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Password2Box);
            this.Controls.Add(this.Password1Box);
            this.Controls.Add(this.UsernameBox);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UsernameBox;
        private System.Windows.Forms.TextBox Password1Box;
        private System.Windows.Forms.TextBox Password2Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label status_label;
        private System.Windows.Forms.Label UsernameStatus;
        private System.Windows.Forms.Label PasswordStatus;
        private System.Windows.Forms.Label PasswordRetryStatus;
    }
}